package com.academy.cleancode;


public class Car {

    private int radioFrequency;
    private int secretCode;

    public Car(int radioFrequency, int secretCode) {
        this.radioFrequency = radioFrequency;
        this.secretCode = secretCode;
    }

    public boolean startCar(Fob fob){
        return fob.isValid(this.radioFrequency, this.secretCode);
    }


}
