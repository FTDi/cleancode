package com.academy.cleancode;

public class Fob {
    private int secretKey;
    private String serialNumber;
    private String manufacturer;
    private int radioFrequency;

    public Fob(String serialNumber, String manufacturer, int radioFrequency, int secretKey) {
        this.serialNumber = serialNumber;
        this.manufacturer = manufacturer;
        this.radioFrequency = radioFrequency;
        this.secretKey = secretKey;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getRadioFrequency() {
        return radioFrequency;
    }

    public boolean isValid(int frequency, int secretKey){
        return this.radioFrequency == frequency && this.secretKey == secretKey;
    }
}
