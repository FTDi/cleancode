package com.academy.cleancode;

public interface Engine {

    int getRevs();
}
