package com.academy.cleancode;

import org.junit.Assert;
import org.junit.Test;

public class SomeClassTest {

    @Test
    public void test_0_0_should_return_love_all(){
        SomeClass someClass = new SomeClass("Jonas", "Petras", 0,0);
        Assert.assertEquals("Love-Alle", someClass.getScore());
    }

    @Test
    public void test_0_0_should_return_fifteen_love(){
        Assert.assertEquals(1,1);
    }

    @Test
    public void test_0_0_should_return_deuce(){
        SomeClass someClass = new SomeClass("Jonas", "Petras", 3,3);
        Assert.assertEquals("Deuce", someClass.getScore());
    }
}
